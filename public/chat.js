const socket = io()

let username = localStorage.getItem('pseudo')
const messages = document.getElementById('messages')
const form = document.getElementById('form')
const input = document.getElementById('input')

// Demande du pseudo utilisateur (si pas dans le localstorage)
while (!username) {
    username = prompt('Quel est votre pseudo')
    localStorage.setItem('pseudo', username)
}


// Envoi du nouveau user
socket.emit('newUser', username)

// Envoi d'un message
form.addEventListener('submit', function(e) {
    e.preventDefault()
    if (input.value) {
        const msg = input.value;
        const date = new Date();
        // moment().startOf('hour').fromNow(); 
        socket.emit('newMessage', username, msg, date)
        input.value = ''
    }
})

// Récupération des nouveaux messages
socket.on('newMessage', function(username, msg, date) {
    var item = document.createElement('li')
    item.textContent = username + ': ' + msg + ' - ' + date
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération des nouvelles notification
socket.on('newNotification', function(msg) {
    var item = document.createElement('li')
    item.classList = 'notif'
    item.textContent = msg
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération de la liste des utilisateurs
socket.on('updateUsersList', function(users) {
    // Mettre à jour la liste dans le DOM (document) => Page html
    console.log(users, 'Users list');

    for(let i = 0 ; i < users.length; i++){
            console.log(users[i].username);
            let node = document.createElement('LI');
            let textnode = document.createTextNode(users[i].username);
            node.appendChild(textnode);
            document.getElementById('users').appendChild(node);
    }
})